(ns crud-pat-demo.handlers.patients
  (:require [crud-pat-demo.db.patients :as pdb]
            [taoensso.timbre :as timbre]
            [java-time :as jt]))

(defn all-patients [{{filters :query} :parameters} boundary]
  (let [with-date-filters (when (:birthday filters) (update filters :birthday jt/local-date))
        patients (pdb/all-patients boundary (or with-date-filters filters))]
    (timbre/debug "Got patients" patients "with filters" filters)
    {:status 200
     :body patients}))

(defn patient-by-id [{{{:keys [patient-id]} :path} :parameters} boundary]
  (if-let [patient (pdb/patient boundary patient-id)]
    (do
      (timbre/debug "Got patient" patient)
      {:status 200
       :body patient})
    (do
      (timbre/debug "Patient with such ID does not exist" patient-id)
      {:status 404})))

(defn create-patient [{{{:keys [fio sex birthday address polis_number]} :body} :parameters} boundary]
  (let [birthday-date (java.time.LocalDate/parse birthday)
        patient (pdb/create-patient! boundary fio sex birthday-date address polis_number)]
    {:status 201
     :body patient}))


(defn update-patient [{{{:keys [patient-id]} :path patient :body} :parameters} boundary]
  (let [with-birthday-date (when (:birthday patient) (update patient :birthday jt/local-date))
        patient (pdb/update-patient! boundary patient-id with-birthday-date)]
    (if patient
      (do
        (timbre/debug "Patient updated" patient)
        {:status 200
         :body patient})
      (do
        (timbre/debug "Patient for update not found with ID" patient-id)
        {:status 404
         :body "Patient with such ID does not exist"}))))

(defn delete-patient [{{{:keys [patient-id]} :path} :parameters} boundary]
  (if-let [patient (pdb/delete-patient! boundary patient-id)]
    (do
      (timbre/debug "Patient is found with ID" patient-id ":" patient)
      {:status 200
       :body patient})
    (do
      (timbre/debug "Patient not found with ID" patient-id)
      {:status 404
       :body "Patient with such ID does not exist"})))
