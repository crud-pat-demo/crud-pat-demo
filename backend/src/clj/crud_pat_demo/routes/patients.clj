(ns crud-pat-demo.routes.patients
  (:require [muuntaja.core :as m]
            [clojure.spec.alpha :as s]
            [reitit.ring :as ring]
            [ring.middleware.cors :refer [wrap-cors]]
            [reitit.coercion.spec]
            [reitit.ring.coercion :as rrc]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.ring.middleware.exception :as exception]
            [reitit.dev.pretty :as pretty]
            [crud-pat-demo.handlers.patients :as hp]
            [java-time :as jt]
            [taoensso.timbre :as timbre]))

(s/def ::id int?)
(s/def ::fio (s/and string? seq))
(s/def ::sex (s/and string? seq))
(s/def ::birthday (s/or :d #(instance? java.time.LocalDate %)
                        :s (s/and string?
                                  seq
                                  #(try
                                     (java.time.LocalDate/parse %)
                                     true
                                     (catch Exception e false)))))
(s/def ::address (s/or :n nil? :s (s/and string? seq)))
(s/def ::polis_number (s/and string? seq))
(s/def ::is_deleted boolean?)
(s/def ::patient (s/keys :req-un [::fio ::sex ::birthday ::polis_number]
                         :opt-un [::id ::address ::is_deleted]))
(s/def ::patients (s/coll-of ::patient :min-count 0))

(defn allow-cross-origin
  "middleware function to allow cross origin"
  [handler]
  (fn [request]
    (let [response (handler request)]
      (-> response
       (assoc-in [:headers "Access-Control-Allow-Origin"]  "http://crud-pat:8081")
       (assoc-in [:headers "Access-Control-Allow-Credentials"] "true")
       (assoc-in [:headers "Access-Control-Allow-Methods"] "GET,PUT,POST,DELETE,OPTIONS")
       (assoc-in [:headers "Access-Control-Allow-Headers"] "X-Requested-With,Content-Type,Cache-Control")))))

(defn patients [boundary]
  (ring/ring-handler
    (ring/router
     ["/patients" 
      ["" {:get {:parameters {:query
                              (s/keys :opt-un [::id ::fio ::sex ::birthday
                                               ::polis_number ::address ::is_deleted])}
                 :responses {200 {:body ::patients}}
                 :handler (fn [request] (hp/all-patients request boundary))}}]
      ["/create" {:post {:parameters {:body ::patient}
                         :responses {201 {:body ::patient}}
                         :handler (fn [request] (hp/create-patient request boundary))}
                  :conflicting true}]
      ["/:patient-id" {:get {:parameters {:path {:patient-id int?}}
                             :responses  {200 {:body ::patient}}
                             :handler    (fn [request] (hp/patient-by-id request boundary))}
                       :post {:parameters {:path {:patient-id int?}
                                           :body (s/keys :opt-un [::id ::fio ::sex ::birthday
                                                                  ::polis_number ::address ::is_deleted])}
                              :responses {200 {:body ::patient}
                                          404 {:body string?}}
                              :handler (fn [request] (hp/update-patient request boundary))}
                       :delete {:parameters {:path {:patient-id int?}}
                                :responses {200 {:body ::patient}
                                            404 {:body string?}}
                                :handler (fn [request] (hp/delete-patient request boundary))}
                       :conflicting true}]]
      ;; router data affecting all routes
      {:data {:coercion   reitit.coercion.spec/coercion
              :muuntaja   m/instance
              :middleware [exception/exception-middleware
                           allow-cross-origin
                           muuntaja/format-middleware
                           rrc/coerce-exceptions-middleware
                           parameters/parameters-middleware
                           rrc/coerce-request-middleware
                           muuntaja/format-response-middleware
                           rrc/coerce-response-middleware]}
       :exception  pretty/exception})))
