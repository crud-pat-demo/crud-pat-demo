(ns crud-pat-demo.db.patients
  (:require [clojure.core :as c]
            [integrant.core :as ig]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]
            [next.jdbc.date-time :as date-time]
            [honey.sql :as sql]
            [honey.sql.helpers :refer :all :as h]
            [taoensso.timbre :as timbre]))

(defrecord PatientBoundary [data-source])

(defmethod ig/init-key :database/postgres [_ db-config]
  (let [ds (jdbc/get-datasource db-config)]
    (date-time/read-as-local)
    (PatientBoundary. ds)))

(defprotocol Boundary
  (execute! [{data-source :data-source} cmd] "исполняет SQL команду")
  (execute-one! [{data-source :data-source} cmd] "исполняет SQL команду"))

(extend-type PatientBoundary
  Boundary
  (execute! [{data-source :data-source} cmd]
    (jdbc/execute! data-source cmd
                   {:builder-fn rs/as-unqualified-lower-maps
                    :return-keys true}))
  (execute-one! [{data-source :data-source} cmd]
    (jdbc/execute-one! data-source cmd
                       {:builder-fn rs/as-unqualified-lower-maps
                        :return-keys true})))

(defprotocol AllPatients
  (all-patients [this filters?] "получение всех пациентов"))

(def ^:private patients-cmps
  {:id #(vector := :id %)
   :fio #(vector :ilike :fio (str "%" % "%"))
   :sex #(vector :ilike :sex (str "%" % "%"))
   :address #(vector :ilike :address (str "%" % "%"))
   :birthday #(vector :> :birthday %)
   :polis_number #(vector :ilike :polis_number (str "%" % "%"))
   :is_deleted #(vector := :is_deleted %)})

(defn merge-filters [values]
  (if (zero? (count values))
    []
    (->> values
         (c/filter (fn [[k _]] (get patients-cmps k)))
         (mapv (fn [[k v]] ((get patients-cmps k) v)))
         (apply conj [:and]))))

(extend-type PatientBoundary
  AllPatients
  (all-patients [this filters?]
    (timbre/info "Getting patients with filters " filters?)
    (let [filters (if-not (nil? filters?) (merge-filters filters?) [])
          sqlmap {:select :*
                  :from :patients
                  :where filters}]
      (execute! this (sql/format sqlmap)))))

(defprotocol GetPatient
  (patient [this id] "получене пациента по его ID"))

(extend-type PatientBoundary
  GetPatient
  (patient [this id]
    (timbre/info "Getting patient with ID: " id)
    (let [sqlmap {:select :*
                  :from :patients
                  :where [:= :id id]}]
      (execute-one! this (sql/format sqlmap)))))

(defprotocol CreatePatient
  (create-patient! [this full-name sex birthday-date address? polis-number] "создание нового пациента"))

(extend-type PatientBoundary
  CreatePatient
  (create-patient! [this full-name sex birthday-date address? polis-number]
    (timbre/info "Creating patient with [" full-name sex birthday-date address? polis-number "]")
    (let [sqlmap {:insert-into :patients
                  :columns [:fio :sex :birthday :address :polis_number]
                  :values [[full-name sex birthday-date address? polis-number]]}]
      (execute-one! this (sql/format sqlmap)))))

(defprotocol UpdatePatient
  (update-patient! [this id update-map] "обновление пациента по его ID"))

(extend-type PatientBoundary
  UpdatePatient
  (update-patient! [this id update-map]
    (let [sqlmap {:update :patients
                  :set (dissoc update-map :id :is-deleted)
                  :where [:= :id id]}]
      (timbre/info "Updating patient with ID" id "with data" sqlmap)
      (execute-one! this (sql/format sqlmap)))))

(defprotocol DeletePatient
  (delete-patient! [this id] "удаление пациента по его ID"))

(extend-type PatientBoundary
  DeletePatient
  (delete-patient! [this id]
    (timbre/info "Deleting patient with ID " id)
    (let [sqlmap {:update :patients
                  :set {:is-deleted true}
                  :where [:= :id id]}]
      (execute-one! this (sql/format sqlmap)))))
