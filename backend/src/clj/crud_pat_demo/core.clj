(ns crud-pat-demo.core
  (:require [integrant.core :as ig]
            [clojure.tools.nrepl.server :as serv]
            [clojure.java.io :as io]
            [org.httpkit.server :as server]
            [aero.core :as a]
            [crud-pat-demo.routes.patients :as p]
            [crud-pat-demo.db.patients :as pdb]
            [crud-pat-demo.handlers.patients :as hp]
            [taoensso.timbre.appenders.core :as appenders]
            [taoensso.timbre :as timbre])
  (:gen-class))

(defmethod ig/init-key :adapter/http-kit [_ {boundary :boundary}]
  {:server-stop-fn (server/run-server (p/patients boundary) {:port 8080})})

(defmethod ig/halt-key! :adapter/http-kit [_ {server-stop-fn :server-stop-fn :as p}]
  (when server-stop-fn
    (server-stop-fn))
  (dissoc p :server-stop-fn))

(defmethod ig/init-key :log/timbre [_ {log-file :log-file}]
  (when (seq log-file)
    (timbre/merge-config!
     {:appenders {:spit (appenders/spit-appender {:fname log-file})}})))

(defmethod aero.core/reader 'ig/ref
  [{:keys [profile] :as opts} tag value]
  (integrant.core/ref value))

(defn read-config [resource-file-name]
  (->> resource-file-name
       io/resource
       a/read-config))

(defn env
  ([s d]
   (try
     (System/getenv s)
     (catch Exception e d)))
  ([s]
   (env s nil)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (->> "config.edn"
       read-config
       ig/init))
