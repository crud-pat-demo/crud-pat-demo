(ns crud-pat-demo.table
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [reagent.dom :as rdom]
   [reagent.core :as r]
   [cljs-http.client :as http]
   [cljs.core.async :refer [<!]]
   [cljs-time.local :as lt]))

(defn string-includes? [str substr]
  (clojure.string/includes?
   (clojure.string/lower-case str)
   (clojure.string/lower-case substr)))

(def ^:private local-date-formatter (cljs-time.format/formatter-local "yyyy-MM-dd"))
(defn- unparse [date] (cljs-time.format/unparse local-date-formatter date))
(defn- date-now [] (unparse (lt/local-now)))

(defn filter-field [valid-fn on-change]
  (let [filter-value (r/atom "")]
    (fn []
      [:input.input-field
       {:type "text"
        :class ["filter"
                (if-not (or (not (seq @filter-value))
                            (valid-fn @filter-value)) "not-valid")]
        :value @filter-value
        :on-change (fn [value]
                     (let [new-value (-> value .-target .-value)]
                       (reset! filter-value new-value)
                       (on-change new-value)))}])))

(defn date-filter-field [valid-fn on-change]
  (let [filter-value (r/atom "")]
    (fn []
      [:input.input-field
       {:type "date"
        :class ["filter"
                (if-not (or (not (seq @filter-value))
                            (valid-fn @filter-value)) "not-valid")]
        :min "1900-01-01"
        :max (date-now)
        :value @filter-value
        :on-change (fn [value]
                     (let [new-value (-> value .-target .-value)]
                       (reset! filter-value new-value)
                       (on-change new-value)))}])))


(defn named-filter [{:keys [key type valid? on-change] :or {valid? (constantly true)}}]
  [filter-field
   valid?
   (fn [new-value]
     (on-change
      (if (and (seq new-value) (valid? new-value))
        #(assoc % key new-value)
        #(dissoc % key))))])

(defn date-named-filter [{:keys [key type valid? on-change] :or {valid? (constantly true)}}]
  [date-filter-field
   valid?
   (fn [new-value]
     (on-change
      (if (and (seq new-value) (valid? new-value))
        #(assoc % key new-value)
        #(dissoc % key))))])

(defn id-filter [filters on-change-filter]
  [named-filter
   {:key :id
    :filters filters
    :valid? #(parse-long %)
    :on-change on-change-filter}])

(defn fio-filter [filters on-change-filter]
  [named-filter
   {:key :fio
    :filters filters
    :on-change on-change-filter}])

(defn sex-filter [filters on-change-filter]
  [named-filter
   {:key :sex
    :filters filters
    :on-change on-change-filter}])

(defn birthday-filter [filters on-change-filter]
  [date-named-filter
   {:key :birthday
    :filters filters
    :on-change on-change-filter}])

(defn address-filter [filters on-change-filter]
  [named-filter
   {:key :address
    :filters filters
    :on-change on-change-filter}])

(defn polis-filter [filters on-change-filter]
  [named-filter
   {:key :polis_number
    :filters filters
    :on-change on-change-filter}])

(defn filters-head [filters on-change-filter]
  [:thead.buttom-border-line
    [:tr
     [:th [id-filter filters on-change-filter]]
     [:th [fio-filter filters on-change-filter]]
     [:th [sex-filter filters on-change-filter]]
     [:th [birthday-filter filters on-change-filter]]
     [:th [address-filter filters on-change-filter]]
     [:th [polis-filter filters on-change-filter]]]])

(def filter-fields
  (vector id-filter fio-filter sex-filter
          birthday-filter address-filter polis-filter))

(defn patients-table [{:keys [patients filters on-select on-change-filter]}]
  [:table.patients-table {:cellPadding 0}
   [:thead
    [:tr
     [:th "Id"]
     [:th "Full name"]
     [:th "Sex"]
     [:th "Birthday"]
     [:th "Address"]
     [:th "Polis"]]]
   [filters-head filters on-change-filter]
   [:tbody.scrollable-patients
    (for [item patients]
      ^{:key (:id item)}
      [:tr
       {:on-click (fn [_] (on-select item))
        :class ["patient-row" (if (get item :selected? false) "selected")
                (if (get item :is_deleted false) "deleted")]}
       (map-indexed
        (fn [i k]
          ^{:key (or (k item) i)}
          [:td (get item k "null")])
        '(:id :fio :sex :birthday :address :polis_number))])]])

(defn filter-xform [selected?]
  (comp
   (map #(assoc % :selected? (selected? %)))))

(defn highlight-selected-patient [coll selected?]
  (transduce (filter-xform selected?) conj [] coll))

(defn foot-bar [{:keys [on-create]}]
  [:input.button.create-patient
   {:type "button"
    :value "Создать пациента"
    :on-click on-create}])

(defn- sort-patients [patients]
  (sort #(case [(:is_deleted %1) (:is_deleted %2)]
           [true true] (< (:id %1) (:id %2))
           [true false] false
           [false true] true
           [false false] (< (:id %1) (:id %2))) patients))

(defn table [{:keys [patients selected filters on-change-filter on-select on-create]}]
  (let [selected-filter #(= (:id selected) (:id %))
        patients (sort-patients patients)
        highlighted (highlight-selected-patient patients selected-filter)]
    [:div
     [patients-table {:patients highlighted
                      :filters filters
                      :on-select on-select
                      :on-change-filter on-change-filter}]
     [foot-bar {:on-create on-create}]]))
