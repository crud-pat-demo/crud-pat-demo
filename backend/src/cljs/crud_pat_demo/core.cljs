(ns crud-pat-demo.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [reagent.dom :as rdom]
   [reagent.core :as r]
   [cljs-http.client :as http]
   [cljs.core.async :refer [<! timeout]]
   [crud-pat-demo.table :as t]
   [crud-pat-demo.form :as form]))

(defn fetch-patients [filters on-data on-error]
  (go (let [response (<! (http/get "http://crud-pat:80/api/patients"
                                   {:query-params filters}))]
        (if (= 200 (:status response))
          (on-data (:body response))
          (on-error {:status response})))))

(defn create-patient [patient on-data on-error]
  (go (let [response (<! (http/post "http://crud-pat:80/api/patients/create"
                                    {:json-params patient}))]
        (if (= 201 (:status response))
          (on-data (:body response))
          (on-error {:status response})))))

(defn update-patient [patient on-data on-error]
  (let [id (:id patient)]
    (go (let [response (<! (http/post (str "http://crud-pat:80/api/patients/" id)
                                      {:json-params patient}))]
          (if (= 200 (:status response))
            (on-data (:body response))
            (on-error {:status response}))))))

(defn delete-patient [id on-data on-error]
  (go (let [response (<! (http/delete (str "http://crud-pat:80/api/patients/" id)))]
        (if (= 200 (:status response))
          (on-data (:body response))
          (on-error {:status response})))))

(defn update-patients [filters on-update-patients]
  (fetch-patients filters on-update-patients println))

(defn on-filter-changed [filters on-update-filters]
  (fn [f]
    (let [new-filters (f @filters)]
      (reset! filters new-filters)
      (on-update-filters new-filters))))

(defn bounce-update-patients [request]
  (let [timer (atom nil)]
    (fn [filters]
      (js/clearTimeout @timer)
      (reset! timer (js/setTimeout #(request filters) 2000)))))

(defn bouncer-fn [on-update-patients]
  (bounce-update-patients
   #(update-patients % on-update-patients)))

(defn patient-form [{:keys [selected-patient on-close on-create on-update on-delete]}]
  [:div.form
   [form/patient-form
    {:current-status selected-patient
     :on-close on-close
     :on-update on-update
     :on-create on-create
     :on-delete on-delete}]])

(defn page []
  (let [selected-patient (r/atom nil)
        patients (r/atom [])
        filters (r/atom {})
        update-patients! #(reset! patients %)
        bounce-fn (bouncer-fn update-patients!)
        on-filter-changed-handler (on-filter-changed filters bounce-fn)]
    (update-patients @filters update-patients!)
    (fn []
      [:div.page
       [:div.table
        [t/table
         {:patients @patients
          :filters filters
          :on-change-filter on-filter-changed-handler
          :on-select #(reset! selected-patient
                              {:status :update
                               :current-patient %})
          :selected (:current-patient @selected-patient)
          :on-create #(reset! selected-patient
                              {:status :create
                               :current-patient {}})}]]
       (if @selected-patient
         [patient-form {:selected-patient selected-patient
                        :on-close #(reset! selected-patient nil)
                        :on-create #(create-patient
                                     %
                                     (fn [patient]
                                       (reset! selected-patient
                                               {:status :update
                                                :current-patient patient})
                                       (update-patients @filters update-patients!))
                                     println)
                        :on-update #(update-patient
                                     %
                                     (fn [patient]
                                       (reset! selected-patient
                                               {:status :update
                                                :current-patient patient})
                                       (update-patients @filters update-patients!))
                                     println)
                        :on-delete #(delete-patient
                                     %
                                     (fn [patient]
                                       (reset! selected-patient
                                               {:status :update
                                                :current-patient patient})
                                       (update-patients @filters update-patients!))
                                     println)}]
         [:div.form])])))

(defn ^:dev/after-load mount-components []
  (rdom/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (mount-components))

(init!)
