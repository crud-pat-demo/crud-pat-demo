(ns crud-pat-demo.form
  (:require
   [reagent.dom :as rdom]
   [reagent.core :as r]))

(defn text-form-field [{:keys [text disabled? value on-change error? required?]
                        :or {disabled? false}}]
  [:div
   [:div text (if required? [:span.red "*"])]
   [:input.input-field
    {:type "text"
     :disabled disabled?
     :class [(if disabled? "disabled") (if error? "not-valid")]
     :value value
     :on-change #(let [new-value (-> % .-target .-value)]
                   (on-change new-value))}]])

(defn date-form-field [{:keys [text disabled? value on-change error? required?]
                        :or {disabled? false}}]
  [:div
   [:div text (if required? [:span.red "*"])]
   [:input
    {:type "date"
     :disabled disabled?
     :class [(if disabled? "disabled") (if error? "not-valid") "input-field"]
     :value value
     :on-change #(let [new-value (-> % .-target .-value)]
                   (on-change new-value))}]])

(defn patient-form-fields [buffer-patient on-error]
  (let [fio-error? (empty? (:fio @buffer-patient))
        sex-error? (empty? (:sex @buffer-patient))
        birthday-error? (empty? (:birthday @buffer-patient))
        polis-error? (empty? (:polis_number @buffer-patient))]
    (on-error (or fio-error? sex-error? birthday-error? polis-error?))
    [:div.form-content-fields
     [text-form-field {:text "ID" :disabled? true :value (:id @buffer-patient)
                       :on-change #(swap! buffer-patient assoc :id %)}]
     [text-form-field {:text "Full name" :required? true :value (:fio @buffer-patient)
                       :error? fio-error?
                       :on-change #(swap! buffer-patient assoc :fio %)}]
     [text-form-field {:text "Sex" :required? true :value (:sex @buffer-patient)
                       :error? sex-error?
                       :on-change #(swap! buffer-patient assoc :sex %)}]
     [text-form-field {:text "Address" :value (:address @buffer-patient)
                       :on-change #(swap! buffer-patient assoc :address %)}]
     [date-form-field {:text "Birthday" :required? true :value (:birthday @buffer-patient)
                       :error? birthday-error?
                       :on-change #(swap! buffer-patient assoc :birthday %)}]
     [text-form-field {:text "Polis" :required? true :value (:polis_number @buffer-patient)
                       :error? polis-error?
                       :on-change #(swap! buffer-patient assoc :polis_number %)}]]))

(defn form-create-buttons [{:keys [on-close on-create]}]
  [:div.form-buttons
   [:input.button.button-close.form-button
    {:type "button"
     :value "Закрыть"
     :on-click on-close}]
   [:input.button.button-update.form-button
    {:type "button"
     :value "Создать"
     :on-click on-create}]])

(defn form-update-buttons [{:keys [error? on-close on-delete on-update deleted?]}]
  [:div.form-buttons
   [:input.button.button-close.form-button
    {:type "button"
     :value "Закрыть"
     :on-click on-close}]
   [:input.button.button-delete.form-button
    {:type "button"
     :disabled deleted?
     :value "Удалить"
     :on-click on-delete}]
   [:input.button.button-update.form-button
    {:type "button"
     :disabled @error?
     :value "Обновить"
     :on-click on-update}]])

(defn patient-form [{:keys [current-status on-close on-create on-update on-delete]}]
  (let [{:keys [status current-patient]} @current-status
        buffer-patient (r/atom current-patient)
        error? (r/atom false)]
    (println current-patient)
    [:div.form-content
     [patient-form-fields buffer-patient #(reset! error? %)]
     (if (= status :create)
       [form-create-buttons {:on-close on-close
                             :on-create #(on-create @buffer-patient)}]
       [form-update-buttons {:error? error?
                             :on-delete #(on-delete (:id @buffer-patient))
                             :on-update #(on-update @buffer-patient)
                             :on-close on-close
                             :deleted? (:is_deleted current-patient)}])]))
