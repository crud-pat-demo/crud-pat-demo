(defproject crud-pat-demo "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/clojurescript "1.11.54"
                  :scope "provided"]
                 [org.clojure/core.async "1.5.648"]
                 [org.clojure/tools.logging "1.2.4"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [org.postgresql/postgresql "42.3.5"]
                 [ring "1.9.5"]
                 [ring-cors "0.1.13"]
                 [http-kit "2.3.0"]
                 [aero "1.1.6"]
                 [reagent "1.1.1"]
                 [metosin/reitit "0.5.18"]
                 [integrant "0.8.0"]
                 [cljs-http "0.1.46"]
                 [com.github.seancorfield/next.jdbc "1.2.780"]
                 [com.github.seancorfield/honeysql "2.2.891"]
                 [com.taoensso/timbre "5.2.1"]
                 [clojure.java-time "0.3.3"]
                 [thheller/shadow-cljs "2.19.0"
                  :scope "provided"]
                 [com.andrewmcveigh/cljs-time "0.5.2"]]
  :source-paths ["src/clj" "src/cljs"]
  :resource-paths ["resources"]
  :main ^:skip-aot crud-pat-demo.core
  :target-path "target/%s"
  :jar-name "crud-pat-demo.jar"
  :uberjar-name "crud-pat-demo-uberjar.jar"
  :clean-targets ^{:protect false}
  [:target-path "resources/public/js/out"]
  :plugins [[refactor-nrepl "3.5.2"]
            [cider/cider-nrepl "0.28.3"]]
  :cljsbuild
  {:builds {:min
            {:source-paths ["src/cljs"]
             :compiler
             {:output-to        "resources/public/js/app.js"
              :output-dir       "resources/public/js"
              :source-map       "resources/public/js/app.js.map"
              :optimizations :advanced
              :infer-externs true
              :pretty-print  false}}
            :app
            {:source-paths ["src/cljs"]
             :compiler
             {:main "crud_pat_demo.dev"
              :asset-path "/js/out"
              :output-to "resources/public/js/app.js"
              :output-dir "resources/public/js/out"
              :source-map true
              :optimizations :none
              :pretty-print  true}}}}
  :profiles {:user {:plugins [[cider/cider-nrepl "0.28.3"]]} 
             :cljs {:plugins [[cider/cider-nrepl "0.28.3"]
                              [lein-cljsbuild "1.1.8"]]
                    :source-paths ["dev/cljs"]
                    :resource-paths ["resources/public"]}
             :dev {:plugins [[cider/cider-nrepl "0.28.3"]
                             [refactor-nrepl "3.5.2"]]
                   :dependencies [[integrant/repl "0.3.2"]
                                  [org.clojure/tools.namespace "1.2.0"]]
                   :source-paths ["dev/clj"]}
             :uberjar {:aot :all
                       :prep-tasks ["compile" ["run" "-m" "shadow.cljs.devtools.cli" "release" "app"]]
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
