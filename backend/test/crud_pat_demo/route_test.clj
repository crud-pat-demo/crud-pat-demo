(ns crud-pat-demo.route-test
  (:require [clojure.test :refer :all]
            [crud-pat-demo.routes.patients :as pr]
            [crud-pat-demo.db.patients :as pd]
            [crud-pat-demo.patients-test :refer :all]
            [reitit.core :as r]
            [muuntaja.core :as m]))

(defn test-all-patients [coll]
  (let [stub (reify pd/AllPatients
               (all-patients [this filters?] coll))
          routes (pr/patients stub)
          request {:request-method :get :uri "/patients"}
          result (routes request)
          status (:status result)
          body (m/decode-response-body result)]
    {:status status :body body}))

(defn test-get-patient [id patient]
  (let [stub (reify pd/GetPatient
               (patient [this id] patient))
        routes (pr/patients stub)
        request {:request-method :get :uri "/patients/8"
                 :path-params {:patient-id 8}}
        result (routes request)
        status (:status result)
        body (m/decode-response-body result)]
    {:status status :body body}))

(defn test-get-patient-not-found [id patient]
  (let [stub (reify pd/GetPatient
               (patient [this id] patient))
        routes (pr/patients stub)
        request {:request-method :get :uri "/patients/8"
                 :path-params {:patient-id 8}}
        result (routes request)
        status (:status result)]
    {:status status}))

(defn test-create-patient [patient]
  (let [stub (reify pd/CreatePatient
               (create-patient! [this fio sex birthday-date address? polis-number] patient))
        routes (pr/patients stub)
        request {:request-method :post :uri "/patients/create"
                 :body-params patient}
        result (routes request)
        status (:status result)
        body (m/decode-response-body result)]
    {:status status :body body}))


(defn test-update-patient [id patient]
  (let [stub (reify pd/UpdatePatient
               (update-patient! [this id update-map] patient))
        routes (pr/patients stub)
        request {:request-method :post :uri (str "/patients/" id)
                 :path-parameters {:patient-id id}
                 :body-params patient}
        result (routes request)
        _ (println result)
        status (:status result)
        body (m/decode-response-body result)]
    {:status status :body body}))

(defn test-update-patient-not-found [id patient]
  (let [stub (reify pd/UpdatePatient
               (update-patient! [this id update-map] nil))
        routes (pr/patients stub)
        request {:request-method :post :uri (str "/patients/" id)
                 :path-parameters {:patient-id id}
                 :body-params patient}
        result (routes request)
        status (:status result)]
    status))

(defn test-update-patient-partly [id patient]
  (let [stub (reify pd/UpdatePatient
               (update-patient! [this id update-map] correct-patient-address))
        routes (pr/patients stub)
        request {:request-method :post :uri (str "/patients/" id)
                 :path-parameters {:patient-id id}
                 :body-params patient}
        result (routes request)
        status (:status result)
        body (m/decode-response-body result)]
    {:status status :body body}))

(defn test-delete-patient [id patient]
  (let [stub (reify pd/DeletePatient
               (delete-patient! [this id] patient))
        routes (pr/patients stub)
        request {:request-method :delete :uri (str "/patients/" id)
                 :path-parameters {:patient-id id}}
        result (routes request)
        status (:status result)
        body (m/decode-response-body result)]
    {:status status :body body}))


(defn test-delete-patient-not-found [id]
  (let [stub (reify pd/DeletePatient
               (delete-patient! [this id] nil))
        routes (pr/patients stub)
        request {:request-method :delete :uri (str "/patients/" id)
                 :path-parameters {:patient-id id}}
        result (routes request)
        status (:status result)]
    {:status status}))

(deftest routes-test
  (testing "all-patients"
    (testing "correct patients"
      (are [status body actual] (and (= status (:status actual)) (= body (:body actual)))
        200 [] (test-all-patients [])
        200 [correct-patient-address-absent] (test-all-patients [correct-patient-address-absent])
        200 [correct-patient-address] (test-all-patients [correct-patient-address])))
    (testing "incorrect patients"
      (are [expected actual] (= expected (:status actual))
        500 (test-all-patients [incorrect-patient-fio-nil])
        500 (test-all-patients [incorrect-patient-fio-empty])
        500 (test-all-patients [incorrect-patient-fio-absent])
        500 (test-all-patients [incorrect-patient-sex-nil])
        500 (test-all-patients [incorrect-patient-sex-empty])
        500 (test-all-patients [incorrect-patient-sex-absent])
        500 (test-all-patients [incorrect-patient-birthday-nil])
        500 (test-all-patients [incorrect-patient-birthday-empty])
        500 (test-all-patients [incorrect-patient-birthday-absent])
        500 (test-all-patients [incorrect-patient-birthday-wrong-format])
        500 (test-all-patients [incorrect-patient-polis-nil])
        500 (test-all-patients [incorrect-patient-polis-empty])
        500 (test-all-patients [incorrect-patient-polis-absent]))))
  (testing "get-patient"
    (testing "correct patient"
      (are [status body actual] (and (= status (:status actual)) (= body (:body actual)))
        200 correct-patient-address (test-get-patient 1 correct-patient-address)
        200 correct-patient-address-absent (test-get-patient 1 correct-patient-address-absent)
        200 correct-patient-id-absent (test-get-patient 1 correct-patient-id-absent)))
    (testing "patient not found"
      (are [status actual] (= status (:status actual))
        404 (test-get-patient-not-found 1 nil)))
    (testing "incorrect patient"
      (are [status actual] (= status (:status actual))
        500 (test-get-patient 1 incorrect-patient-fio-nil)
        500 (test-get-patient 1 incorrect-patient-fio-empty)
        500 (test-get-patient 1 incorrect-patient-fio-absent)
        500 (test-get-patient 1 incorrect-patient-sex-nil)
        500 (test-get-patient 1 incorrect-patient-sex-empty)
        500 (test-get-patient 1 incorrect-patient-sex-absent)
        500 (test-get-patient 1 incorrect-patient-birthday-nil)
        500 (test-get-patient 1 incorrect-patient-birthday-empty)
        500 (test-get-patient 1 incorrect-patient-birthday-absent)
        500 (test-get-patient 1 incorrect-patient-birthday-wrong-format)
        500 (test-get-patient 1 incorrect-patient-polis-nil)
        500 (test-get-patient 1 incorrect-patient-polis-empty)
        500 (test-get-patient 1 incorrect-patient-polis-absent))))
  (testing "create patient"
    (testing "correct patient"
      (are [status body result] (and (= status (:status result)) (= body (:body result)))
        201 correct-patient-address (test-create-patient correct-patient-address)
        201 correct-patient-address-absent (test-create-patient correct-patient-address-absent)
        201 correct-patient-id-absent (test-create-patient correct-patient-id-absent)))
    (testing "incorrect patient"
      (are [status result] (= status (:status result))
        400 (test-create-patient incorrect-patient-fio-nil)
        400 (test-create-patient incorrect-patient-fio-empty)
        400 (test-create-patient incorrect-patient-fio-absent)
        400 (test-create-patient incorrect-patient-sex-nil)
        400 (test-create-patient incorrect-patient-sex-empty)
        400 (test-create-patient incorrect-patient-sex-absent)
        400 (test-create-patient incorrect-patient-birthday-nil)
        400 (test-create-patient incorrect-patient-birthday-empty)
        400 (test-create-patient incorrect-patient-birthday-absent)
        400 (test-create-patient incorrect-patient-birthday-wrong-format)
        400 (test-create-patient incorrect-patient-polis-nil)
        400 (test-create-patient incorrect-patient-polis-empty)
        400 (test-create-patient incorrect-patient-polis-absent))))
  (testing "update patient"
    (testing "correct patient"
      (are [status body result] (= {:status status :body body} result)
        200 correct-patient-address (test-update-patient-partly 8 {})
        200 correct-patient-address (test-update-patient 8 correct-patient-address)
        200 correct-patient-address-absent (test-update-patient 8 correct-patient-address-absent)
        200 correct-patient-id-absent (test-update-patient 8 correct-patient-id-absent)
        200 correct-patient-address (test-update-patient-partly 8 correct-patient-update-address)))
    (testing "patient not found"
      (are [status result] (= status result)
        404 (test-update-patient-not-found 8 correct-patient-address)))
    (testing "incorrect patient"
      (are [status result] (= status (:status result))
        400 (test-update-patient 8 incorrect-patient-fio-nil)
        400 (test-update-patient 8 incorrect-patient-fio-empty)
        500 (test-update-patient 8 incorrect-patient-fio-absent)
        400 (test-update-patient 8 incorrect-patient-sex-nil)
        400 (test-update-patient 8 incorrect-patient-sex-empty)
        500 (test-update-patient 8 incorrect-patient-sex-absent)
        400 (test-update-patient 8 incorrect-patient-birthday-nil)
        400 (test-update-patient 8 incorrect-patient-birthday-empty)
        500 (test-update-patient 8 incorrect-patient-birthday-absent)
        400 (test-update-patient 8 incorrect-patient-birthday-wrong-format)
        400 (test-update-patient 8 incorrect-patient-polis-nil)
        400 (test-update-patient 8 incorrect-patient-polis-empty)
        500 (test-update-patient 8 incorrect-patient-polis-absent))))
  (testing "delete patient"
    (testing "correct id"
      (are [status body actual] (and (= status (:status actual)) (= body (:body actual)))
        200 correct-patient-address (test-delete-patient 1 correct-patient-address)
        200 correct-patient-address-absent (test-delete-patient 1 correct-patient-address-absent)
        200 correct-patient-id-absent (test-delete-patient 1 correct-patient-id-absent)))
    (testing "patient not found"
      (are [status actual] (= status (:status actual))
        404 (test-delete-patient-not-found 1)))
    (testing "incorrect patient"
      (are [status actual] (= status (:status actual))
        500 (test-delete-patient 1 incorrect-patient-fio-nil)
        500 (test-delete-patient 1 incorrect-patient-fio-empty)
        500 (test-delete-patient 1 incorrect-patient-fio-absent)
        500 (test-delete-patient 1 incorrect-patient-sex-nil)
        500 (test-delete-patient 1 incorrect-patient-sex-empty)
        500 (test-delete-patient 1 incorrect-patient-sex-absent)
        500 (test-delete-patient 1 incorrect-patient-birthday-nil)
        500 (test-delete-patient 1 incorrect-patient-birthday-empty)
        500 (test-delete-patient 1 incorrect-patient-birthday-absent)
        500 (test-delete-patient 1 incorrect-patient-birthday-wrong-format)
        500 (test-delete-patient 1 incorrect-patient-polis-nil)
        500 (test-delete-patient 1 incorrect-patient-polis-empty)
        500 (test-delete-patient 1 incorrect-patient-polis-absent)))))
