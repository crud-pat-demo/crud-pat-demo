(ns crud-pat-demo.handler-test
  (:require [clojure.test :refer :all]
            [crud-pat-demo.db.patients :as dp]
            [crud-pat-demo.handlers.patients :as hp]
            [crud-pat-demo.patients-test :refer :all]
            [reitit.core :as r]))

(defn- test-all-patients [request patients]
  (hp/all-patients
   request
   (reify dp/AllPatients
     (all-patients [this filters?] patients))))


(defn- test-get-patient [patient-id patient]
  (hp/patient-by-id
   {:parameters {:path {:patient-id patient-id}}}
   (reify dp/GetPatient
     (patient [this id] patient))))

(defn- test-create-patient [patient]
  (hp/create-patient
   {:parameters {:body patient}}
   (reify dp/CreatePatient
     (create-patient! [this full-name sex birthday-date address? polis-number] patient))))

(defn- test-update-patient [patient-id patient]
  (hp/update-patient
   {:parameters
    {:path {:patient-id patient-id}
     :body patient}}
   (reify dp/UpdatePatient
     (update-patient! [this id update-map] patient))))

(defn- test-delete-patient [patient-id patient]
  (hp/delete-patient
   {:parameters {:path {:patient-id patient-id}}}
   (reify dp/DeletePatient
     (delete-patient! [this id] patient))))

(deftest handlers-tests
  (testing "get-all-patients"
    (are [status body result] (= {:status status :body body} result)
      200 [] (test-all-patients {} [])
      200 [correct-patient-address] (test-all-patients {} [correct-patient-address])))
  (testing "get-patient"
    (let [{:keys [status body]} (test-get-patient 8 correct-patient-address)]
      (is (= {:status 200 :body correct-patient-address}
             {:status status :body body})))
    (let [{:keys [status]} (test-get-patient 8 nil)]
      (is (= 404 status))))
  (testing "create-patient"
    (is (= 201 (:status (test-create-patient correct-patient-address))))
    (is (thrown? Exception (test-create-patient incorrect-patient-birthday-empty))))
  (testing "update-patient"
    (are [status result] (= status (:status result))
      200 (test-update-patient 8 correct-patient-address)
      404 (test-update-patient 8 nil))
    (is (thrown? Exception (test-update-patient incorrect-patient-birthday-empty))))
  (testing "delete-patient"
    (are [status result] (= status (:status result))
      200 (test-delete-patient 8 correct-patient-address)
      404 (test-delete-patient 8 nil))))
      
