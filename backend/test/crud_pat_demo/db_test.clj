(ns crud-pat-demo.db-test
  (:require [clojure.test :refer :all]
            [crud-pat-demo.db.patients :as dp]))


(deftest merge-filters-tests
  (testing "empty filters"
    (is (= [] (dp/merge-filters {}))))
  (testing "one filter"
    (is (= [:and [:= :id 4]] (dp/merge-filters {:id 4}))))
  (testing "all filters"
    (let [date (java.time.LocalDate/now)]
      (is (= [:and
              [:= :id 4]
              [:ilike :fio "%4%"]
              [:ilike :sex "%4%"]
              [:ilike :address "%4%"]
              [:> :birthday date]
              [:ilike :polis_number "%4%"]]
             (dp/merge-filters
              {:id 4
               :fio "4"
               :sex "4"
               :address "4"
               :birthday date
               :polis_number "4"}))))))
    
