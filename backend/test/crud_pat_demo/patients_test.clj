(ns crud-pat-demo.patients-test)


(def correct-patient-id-absent
  {:fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"
   :is_deleted true})

(def correct-patient-address-absent
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"
   :is_deleted true})

(def correct-patient-address
  {:id 1
   :fio "Test"
   :sex "MZH"
   :address "df"
   :birthday "2022-08-08"
   :polis_number "2323"
   :is_deleted true})

(def correct-patient-update-address
  {:address "add"})

(def incorrect-patient-fio-nil
  {:id 1
   :fio nil
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-fio-empty
  {:id 1
   :fio ""
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-fio-absent
  {:id 1
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"})


(def incorrect-patient-id-nil
  {:id nil
   :fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-sex-nil
  {:id 1
   :fio "Test"
   :sex nil
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-sex-empty
  {:id 1
   :fio "Test"
   :sex ""
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-sex-absent
  {:id 1
   :fio "Test"
   :birthday "2022-08-08"
   :polis_number "2323"})

(def incorrect-patient-birthday-nil
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday nil
   :polis_number "2323"})

(def incorrect-patient-birthday-empty
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday ""
   :polis_number "2323"})

(def incorrect-patient-birthday-absent
  {:id 1
   :fio "Test"
   :sex "MZH"
   :polis_number "2323"})

(def incorrect-patient-birthday-wrong-format
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday "20022-34-1"
   :polis_number "2323"})

(def incorrect-patient-polis-nil
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number nil})

(def incorrect-patient-polis-empty
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"
   :polis_number ""})


(def incorrect-patient-polis-absent
  {:id 1
   :fio "Test"
   :sex "MZH"
   :birthday "2022-08-08"})

(def incorrect-patient-address-empty
  {:id 1
   :fio "Test"
   :sex "MZH"
   :address ""
   :birthday "2022-08-08"
   :polis_number "2323"})
