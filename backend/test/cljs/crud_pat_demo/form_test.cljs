(ns crud-pat-demo.form-test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [crud-pat-demo.form :as f]))


;;(defn text-form-field [{:key s [text disabled? value on-change error? required?]
;;                       :or {disabled? false}}]
;; [:div
;;  [:div text (if required? [:span.red "*"])]
;;  [:input.input-field
;;   {:type "text"
;;    :disabled disabled?
;;    :class [(if disabled? "disabled") (if error? "not-valid")]
;;    :value value
;;    :on-change #(let [new-value (-> % .-target .-value)]
;;                  (on-change new-value))}]])

(deftest test-patient-form
  (testing "testing form field"
    (is (= [:div [:div "test" [:span.red "*"]]
            [:input {:type "text" :diabled false :class [nil nil "input-field"]
                     :value "t" :on-change f/on-change-value-handler}]]
           (f/text-form-field {:text "test" :value "t" :disabled false :on-change nil :error? nil :required? true})))))

(enable-console-print!)

(run-tests)
