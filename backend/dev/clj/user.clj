(ns user
  (:require [integrant.repl :refer [clear go halt prep init reset reset-all]]
            [integrant.core :as ig]
            [clojure.repl :refer [doc]]
            [clojure.java.io :as io]
            [aero.core :as a]
            [next.jdbc :as jdbc]
            [crud-pat-demo.db.patients :as pdb]))

(defmethod aero.core/reader 'ig/ref
  [{:keys [profile] :as opts} tag value]
  (integrant.core/ref value))

(defn config []
  (->> "config.edn"
       io/resource
       a/read-config))

(integrant.repl/set-prep! config)

(defn db-boundary []
  (->> integrant.repl.state/system
       :database/postgres))

(defn datasource []
  (:data-source (db-boundary)))

(defn get-all-patients []
  (pdb/all-patients (db-boundary) nil))

(defn get-patient [id]
  (pdb/patient (db-boundary) id))

(defn create-patient [fio]
  (pdb/create-patient! (db-boundary) fio "MZH" (java.time.LocalDateTime/now) "hello" "123"))

(defn delete-patient [id]
  (pdb/delete-patient! (db-boundary) id))
