(ns ^:dev/once crud-pat-demo.app
  (:require
    [crud-pat-demo.core :as core]))

(enable-console-print!)

(core/init!)
