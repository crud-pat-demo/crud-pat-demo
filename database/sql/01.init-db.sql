CREATE TABLE patients
(
	id serial PRIMARY KEY,
       	fio varchar(256) NOT NULL,
	sex varchar(32) NOT NULL,
	birthday date NOT NULL,
	address varchar(128),
	polis_number varchar(256) NOT NULL,
	is_deleted boolean NOT NULL DEFAULT FALSE
);
